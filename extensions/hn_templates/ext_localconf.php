<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(static function (string $extKey) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig("<INCLUDE_TYPOSCRIPT: source=\"FILE:EXT:$extKey/Configuration/TSconfig/Page/tsconfig.txt\">");
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig("<INCLUDE_TYPOSCRIPT: source=\"FILE:EXT:$extKey/Configuration/TSconfig/User/tsconfig.txt\">");

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\YouTubeHelper::class]['className']
        = \Hn\HnTemplates\Override\YouTubeHelper::class;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\DataProcessing\GalleryProcessor::class]['className']
        = \Hn\HnTemplates\Override\GalleryProcessor::class;

}, 'hn_templates');
