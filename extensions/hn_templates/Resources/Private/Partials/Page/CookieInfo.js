
const initializeCookieBanner = () => {
    let cookieInfo = document.querySelector('.cookie-info');

    if (cookieInfo) {

        let cookieDefined = document.cookie.indexOf('cookies-accepted=true') >= 0;
        if (!cookieDefined) {
            cookieInfo.classList.remove('cookie-info--hidden');
            document.addEventListener('resize', resizeCookieInfo);
            resizeCookieInfo();
        }

        cookieInfo.querySelector('button').addEventListener('click',function () {
            document.cookie = 'cookies-accepted=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
            cookieInfo.classList.add('cookie-info--hidden');
            document.removeEventListener('resize', resizeCookieInfo);
        });

    }

    function resizeCookieInfo () {
        document.querySelector('body').style.paddingBottom = cookieInfo.clientHeight + 'px';
    }
}

if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', initializeCookieBanner);
} else {
    initializeCookieBanner();
}
