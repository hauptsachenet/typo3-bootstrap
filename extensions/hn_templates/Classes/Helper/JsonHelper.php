<?php

namespace Hn\HnTemplates\Helper;

class JsonHelper
{

    public static function encode($value): string
    {
        try {
            return json_encode($value, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        } catch (\JsonException $jsonException) {
            return '';
        }
    }

}
