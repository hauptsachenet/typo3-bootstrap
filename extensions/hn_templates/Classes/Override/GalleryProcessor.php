<?php

namespace Hn\HnTemplates\Override;


class GalleryProcessor extends \TYPO3\CMS\Frontend\DataProcessing\GalleryProcessor
{
    protected function calculateMediaWidthsAndHeights()
    {
        parent::calculateMediaWidthsAndHeights();

        // by default every gallery item has unlimited height
        // this here limits every item to be  4:3 to the max width of the gallery
        foreach ($this->mediaDimensions as &$d) {
            if (empty($d['width']) || empty($d['height'])) {
                continue;
            }

            $scaleFactor = ($this->galleryData['width'] / 4 * 3) / $d['height'];
            if ($scaleFactor < 1) {
                $d['width'] *= $scaleFactor;
                $d['height'] *= $scaleFactor;
            }
        }
    }
}
