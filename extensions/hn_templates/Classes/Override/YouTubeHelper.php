<?php

namespace Hn\HnTemplates\Override;


use TYPO3\CMS\Core\Resource\File;

class YouTubeHelper extends \TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\YouTubeHelper
{
    public function getMetaData(File $file)
    {
        $metaData = parent::getMetaData($file);
        if (empty($metaData['width']) || empty($metaData['height'])) {
            return $metaData;
        }

        // the dimensions given by youtube are horribly small (about 270p)
        // here an example: https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=fLHPIYNtig4
        // i increase them to 540p (half 1080p) since that is a good default size for an embed code
        $scaleFactor = min(960 / $metaData['width'], 540 / $metaData['height']);
        if ($scaleFactor > 1) {
            $metaData['width'] = (int)round($metaData['width'] * $scaleFactor);
            $metaData['height'] = (int)round($metaData['height'] * $scaleFactor);
        }

        return $metaData;
    }

}
