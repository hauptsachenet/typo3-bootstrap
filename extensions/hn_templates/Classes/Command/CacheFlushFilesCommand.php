<?php

namespace Hn\HnTemplates\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CacheFlushFilesCommand extends Command
{
    protected function configure()
    {
        $this->setDescription('Flush file caches');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $cacheDirPattern = Environment::getVarPath() . '/cache/*/*';
        foreach (glob($cacheDirPattern) as $path) {
            if (!is_dir($path)) {
                continue;
            }
            GeneralUtility::rmdir($path, true);
            GeneralUtility::mkdir($path);
        }
        $io->writeln('Flushed all file caches.');
        return 0;
    }


}
