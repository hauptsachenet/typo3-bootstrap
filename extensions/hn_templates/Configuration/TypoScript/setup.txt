<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hn_templates/Configuration/TypoScript/" extensions="-setup.txt">


config.absRefPrefix = /
config.extTarget = _blank
config.fileTarget = _blank

[applicationContext == "Production"]
    config.ATagParams = rel="noopener"
[else]
    config.ATagParams = rel="noopener nofollow"
    page.meta.robots = noindex, nofollow
    page.meta.googlebot = noindex, nofollow
[end]


[getTSFE().beUserLogin]
    config.linkVars := addToList(ADMCMD_simUser)
[end]

# cache headers would cause the user to have an inconsistent experience
# you can clear the typo3 cache but you can't clear your users browser cache
# prefer to make the static file cache stale
config.sendCacheHeaders = 0

config.cache_clearAtMidnight = 1
config.cache_period = 31536000
[applicationContext == "Development"]
    config.no_cache = 1
    config.admPanel = 1
[else]
    config.no_cache = 0
[end]


# remove default js (because it is inserted within the head = evil) but keep some spam protect
config.removeDefaultJS = 1
config.spamProtectEmailAddresses = ascii


# uncomment sys_language_overlay if you have problems with the wrong language being loaded
# overlay shouldn't be necessary in typo3 8 but extbase is too broken to work without it
# enabling this will reduce performance. There might also be problems in free mode.
# https://docs.typo3.org/typo3cms/TyposcriptReference/8.7/Setup/Config/#sys-language-overlay
#config.sys_language_overlay = 1
