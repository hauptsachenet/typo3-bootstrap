SHELL=/bin/sh
PORT_PREFIX?=50
# this command will poll the health of all containers until none are "starting"
WAIT_FOR_DOCKER=@printf "starting up..."; while [ "`docker-compose ps|grep 'health: starting'`" ]; do printf .; sleep 1; done; echo " everything's running"

start: install web/build
	docker-compose up --detach
	$(WAIT_FOR_DOCKER)

stop:
	docker-compose down --remove-orphans

install: web/typo3conf/LocalConfiguration.php vendor node_modules docker-compose.log web/fileadmin

cc: install
	docker-compose up --detach php
	$(WAIT_FOR_DOCKER)
	-bin/typo3 cache:flush
	bin/typo3 database:updateschema --verbose
	bin/typo3 extension:setup

log:
	docker-compose logs --tail=$(if $(TAIL),$(TAIL),4) --follow

test: install
	docker-compose up --detach php
	$(WAIT_FOR_DOCKER)
	bin/phpunit -c phpunit.unit.xml
	bin/phpunit -c phpunit.functional.xml

lint: node_modules
	bin/phpmd web/typo3conf/ext/hn_*/Classes/ text codesize,design
	bin/yarn stylelint web/typo3conf/ext/hn_*/Resources/Private/**/*.scss
	bin/security-checker

pull: REMOTE_SSH_SOCKET=/tmp/ssh_$(notdir $(CURDIR))
pull: REMOTE_HOST=pXXXXXX@pXXXXXX.mittwaldserver.info
pull: REMOTE_DIR=html/master/current
pull: REMOTE_PHP=php_cli
pull: EXPORT_DB_CMD=$(REMOTE_PHP) $(REMOTE_DIR)/vendor/bin/typo3 database:export --verbose --connection=Default  --exclude "cf_*" --exclude "cache_*" --exclude="*_sessions" --exclude="sys_domain" --exclude="sys_file_processedfile" --exclude="sys_log" --exclude="sys_history"
pull: SSH_CMD=ssh -o ControlPath=$(REMOTE_SSH_SOCKET)
pull: start
	rm -f $(REMOTE_SSH_SOCKET)
	ssh -nNf -o ControlMaster=yes -o ControlPath=$(REMOTE_SSH_SOCKET) -o ControlPersist=3600 $(REMOTE_HOST)
	$(SSH_CMD) - '$(EXPORT_DB_CMD) |gzip --fast' |docker exec -i `docker-compose ps -q db` sh -c "gunzip |mysql -uroot -ppassword database"
	-bin/typo3 cache:flush
	bin/typo3 database:updateschema --verbose
	bin/typo3 extension:setup
	rsync -rtve '$(SSH_CMD)' --delete --max-size=10m --exclude="_*_/" :$(REMOTE_DIR)/web/fileadmin/ web/fileadmin/
	ssh -O exit -o ControlPath=$(REMOTE_SSH_SOCKET) -

###################################################
## starting here, every target is an actual target

# install typo3 if the local configuration is missing
web/typo3conf/LocalConfiguration.php: | vendor docker-compose.log
	docker-compose up --detach php
	$(WAIT_FOR_DOCKER)
	bin/typo3 -v install:setup --site-setup-type=no --web-server-config=none
	bin/typo3 database:updateschema --verbose
    bin/typo3 init:scheduler
    -bin/typo3 init:startpage

vendor: $(wildcard composer.*) | docker-compose.log
	bin/composer install
	touch vendor

node_modules: package.json $(wildcard yarn.lock) | docker-compose.log
	bin/yarn install
	touch node_modules

# restart webpack if config files change
web/build: webpack.config.js .browserslistrc node_modules
	docker-compose stop webpack

web/fileadmin:
	mkdir -p web/fileadmin

# this is somewhat a hack. (although documented: https://www.gnu.org/software/make/manual/html_node/Empty-Targets.html#Empty-Targets)
# The docker build process does not generate a file accessable to me
# but since i don't want to build every time, i touch a "log" file that will keep the mod time.
docker-compose.log: $(wildcard Dockerfile*)
	docker-compose build
	touch docker-compose.log
